import React from 'react';
import {ThemeProvider} from 'styled-components/native';

// Global theme
import theme from './src/global/theme/theme';

// Components
import {Home} from './src/screens/Home';

export default function App() {
  return (
    <ThemeProvider theme={theme}>
      <Home />
    </ThemeProvider>
  );
}
