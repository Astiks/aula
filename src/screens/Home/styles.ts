import styled from 'styled-components/native';

export const Container = styled.View`
  background-color: ${({theme}) => theme.colors.secondary};
  width: 50px;
  height: 50px;
  margin: 50px 20px;
  border-radius: 10px;
  border-color: red;
  border-width: 1px;
  transform: rotate(20deg);
`;
